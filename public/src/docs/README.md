# Projet : Tchat tool

## Informations projet

* Année du projet : **2024**
* UE (matière) : **Systèmes Distribués & Systèmes d’Exploitation**
* Niveau d'année : **L3 Informatique**

## Description 

Ce projet a pour but d'implémenter un outil de chat permettant à plusieurs
utilisateurs de dialoguer.

Le principe général de l'application est qu'un utilisateur dispose d'un compte
(pseudo et mot de passe) lui permettant de se connecter au système de chat. Une
fois connecté, il peut envoyer un message à tous les utilisateurs connectés. Tout
message envoyé est reçu par tous les utilisateurs connectés et affiché dans
l'interface de l'utilisateur.

[![forthebadge](https://forthebadge.com/images/featured/featured-built-with-love.svg)](https://forthebadge.com)

## Fonctionnalités

### Client Chat

- Se connecter au système avec un pseudo et un mot de passe
- Se déconnecter du système
- Afficher la liste des utilisateurs connectés
- Créer un nouveau compte
- Supprimer un compte existant
- Envoyer un message à tous les utilisateurs connectés
- Quitter le client

### Afficheur de Messages

- Afficher les messages reçus des autres utilisateurs dans un terminal dédié

### Partie Centralisée

- Gérer les connexions et déconnexions des clients
- Diffuser les messages à tous les clients connectés
- Traiter les requêtes des clients (connexion, déconnexion, liste des utilisateurs, création/suppression de compte)
- Communiquer avec le gestionnaire de comptes pour les opérations liées aux comptes utilisateurs

## Prérequis

- Compilateur C (gcc)
- Java Development Kit (JDK)
- Apache NetBeans (lancement de la partie Java)

## Compilation

### Client

Dans le répertoire `./public/client/tchat/` :

> ```make```

Cela compilera les exécutables `chat_client` et `afficheur_message`.

### Serveur

Dans le répertoire `./public/client/pc/` :

> ```make```

Cela compilera l'exécutable `main`.

### Gestionnaire de Comptes

Dans le répertoire `gestion_comptes/` :

_img screen resultat_

## Démarrage

- On lance GestionCompteImpl.java en lui donnant un port RMI (avant le lancement dans les arguments).[PC1]
- On lance gestion_requête.c et communication.c en même temps grâce au main, qui s'occupe aussi d'ouvrir les pipes.[PC2]
- On lance ClentRMI.java avec ses arguments : adr ip PC1, port RMI, adr ip PC2, port UDP (toujours avant le lancement).[PC2]
- On lance client.c.[PC3]
- On lance clent_chat.c et afficheur_message.c dans le cas d'une connexion réussie.[PC3]

## Fabriqué avec

[![forthebadge](https://img.shields.io/badge/CLion-000000?style=for-the-badge&logo=clion&logoColor=white)](http://forthebadge.com) [![forthebadge](https://img.shields.io/badge/IntelliJ_IDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white
)](http://forthebadge.com)

* [CLion](https://www.jetbrains.com/fr-fr/clion/) - IDE multiplateforme pour C et C++
* [IntelliJ IDEA](https://www.jetbrains.com/fr-fr/idea/) - IDE Java et Kotlin

## Versions

* **Dernière version stable :** 0.0
* **Dernière version :** 0.0

## Auteurs

* **Neo FAROUX** _alias_ [@nfaroux](https://git.univ-pau.fr/nfaroux)
* **Sylvain CHOLLET** _alias_ [@schollet](https://git.univ-pau.fr/schollet)

## License

Ce projet est sous licence ``MIT License`` - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations