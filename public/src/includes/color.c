#include <stdio.h>
#include <stdarg.h>

/* -------------------------------------------------------------------- CLASSIC -------------------------------------------------------------------- */

// ============================== BLACK ==============================
void fpblack(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[0;30m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== RED ==============================
void fpred(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[0;31m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== GREEN ==============================
void fpgreen(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[0;32m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== YELLOW ==============================
void fpyellow(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[0;33m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== BLUE ==============================
void fpblue(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[0;34m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== PURPLE ==============================
void fppurple(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[0;35m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== CYAN ==============================
void fpcyan(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[0;36m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== WHITE ==============================
void fpwhite(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[0;37m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

/* -------------------------------------------------------------------- BOLD -------------------------------------------------------------------- */

// ============================== BLACK : BOLD ==============================
void fpbblack(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[1;30m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== RED : BOLD ==============================
void fpbred(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[1;31m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== GREEN : BOLD ==============================
void fpbgreen(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[1;32m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== YELLOW : BOLD ==============================
void fpbyellow(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[1;33m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== BLUE : BOLD ==============================
void fpbblue(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[1;34m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== PURPLE : BOLD ==============================
void fpbpurple(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[1;35m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== CYAN : BOLD ==============================
void fpbcyan(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[1;36m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}

// ============================== WHITE : BOLD ==============================
void fpbwhite(const char *format, ...) {
    va_list args;
    va_start(args, format);
    
    // Affichage de la séquence d'échappement pour la mise en surbrillance
    printf("\033[1;37m");
    
    // Affichage du texte formaté avec les arguments variables
    vprintf(format, args);
    
    // Réinitialisation des paramètres de mise en forme
    printf("\033[0m");
    
    va_end(args);
}