#include <stdio.h>
#include <stdlib.h>
#include "configTerminal.h"

void configTerminal(char * title, int axisX, int axisY){

    system("clear");
    
    char cmd[150];
    
    // Formatage pour la console
    sprintf(cmd, "echo -ne '\033]0;▰▰▰▰▰▰▰▰   %s   ▰▰▰▰▰▰▰▰▰\007'", title);
    system(cmd);
    
    // Configuration axes X et Y de la console
    printf("\033[8;%d;%dt", axisY, axisX);

    system("clear");
}