#ifndef CLIENT_TCHAT
#define CLIENT_TCHAT

// ======================== CONST =========================

#define PORT 4444
#define BUFFER_SIZE 1024
#define PIPE_NAME "pipe_client_display"

// ====================== STRUCTURES ======================

/* ----------------------------------------------------- */

// ====================== FONCTIONS ======================

// Creation du socket TCP
int creerSocket();

// Menu afficher au client
void menu(int uxModeDisplay);

#endif