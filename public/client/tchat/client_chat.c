#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <pthread.h>

#include "client_chat.h"
#include "../../src/includes/configTerminal.c"
#include "../../src/includes/color.c"

int isUserConnect = 0;
pthread_t thread_recv;
int sock;

int creerSocket() {
    int sock = 0;
    struct sockaddr_in serv_addr;

    // Création de la socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Erreur lors de la création de la socket");
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Connexion au serveur
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Erreur lors de la connexion au serveur");
        exit(EXIT_FAILURE);
    }

    return sock;
}

void creerPipe() {

    // Création du pipe nommé
    if (mkfifo(PIPE_NAME, 0666) == -1) {
        if (errno != EEXIST) {
            perror("Erreur lors de la création du pipe nommé");
        }
    }

}

void sendMessToPipe(char message[100]) {

    // Ouvrir le pipe nommé en écriture
    int pipe_fd = open(PIPE_NAME, O_WRONLY);
    if (pipe_fd == -1) {
        perror("Erreur lors de l'ouverture du pipe nommé en écriture");
    }

    // Écrire le message dans le pipe
    ssize_t bytes_written = write(pipe_fd, message, strlen(message));
    if (bytes_written == -1) {
        perror("Erreur lors de l'écriture dans le pipe");
        close(pipe_fd);
    }

    close(pipe_fd);

}

void* recv_thread(void* arg) {
    char message[1024];
    while (1) {
        memset(message, 0, sizeof(message));
        if (recv(sock, message, sizeof(message), 0) <= 0) {
            // Gérer les erreurs de réception
            break;
        }
        sendMessToPipe(message);
    }
    pthread_exit(NULL);
}

void menu(int uxModeDisplay){

    // ========== Design de l'Interface User ==========

    fpbwhite("\n\n▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰\n\n");
    fpbwhite("\t\t\t\tMENU - APPLICATION (MULTI TCHAT)");
    fpbwhite("\n\n▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰\n\n\n");

    fpwhite("\n > CONNEXION RESEAUX : ");fpwhite("[NORMALE]");
    fpwhite("\n > SECURITE SYSTEME  : ");fpwhite("[ROBUSTE]");
    fpwhite("\n > FLUX UTILISATEUR  : ");fpwhite("[FAIBLE]");
    if (uxModeDisplay == 1){
        fpgreen("\n\n > STATUT CONNEXION  : ");fpgreen("[CONNECTE]\n\n\n\n");
    }
    else {
        fpred("\n\n > STATUT CONNEXION  : ");fpred("[DECONNECTE]\n\n\n\n");
    }

    // ========== Initialisation des options pour le menu : Connecté et Hors-Ligne ==========

    char * tabOptionsOffline[3] = {"Créer un nouveau compte utilisateur", "Se connecter au système du tchat avec un compte", "Quitter l'application"};
    char * tabOptionsOnline[5] = {"Créer un nouveau compte utilisateur",  "Se déconnecter du système", "Afficher la liste de tous les utilisateurs présents", "Supprimer un compte dèja existant dans le système", "Quitter l'application"};

    // ========== Affichage des options ==========

    if (uxModeDisplay == 0) for(int i = 0; i < 3; i++) fpbwhite("\t[%d] - %s\n", i+1, tabOptionsOffline[i]);
    else if (uxModeDisplay == 1) for(int i = 0; i < 5; i++) fpbwhite("\t[%d] - %s\n", i+1, tabOptionsOnline[i]);
    else printf("erreur: mode d'affichage");

    printf("\n\n");

}

int main() {

    // ========== Initialisation Variables ==========

    sock = creerSocket();

    char choix_str[50];

    char userName[50]; // User information
    char mdp[50];      // User information

    char message[100]; // User message (send -> all)

    int uxModeDisplay = isUserConnect; // 0 : offline (defaut) | 1 : online

    // ========== Configuration Globale ==========
    
    configTerminal("TERMINAL - Client chat", 100, 30); // Configuration console
    system("clear");

    // ==================== Lancement du processus : Afficheur message ====================

    // Lancer le terminal enfant avec afficheur_message
    #if 1 
    system("gnome-terminal -- ./afficheur_message &"); 
    #endif

    // ==================== Connexion par socket avec le processus Communication ====================

    // connect_server();

    // ==================== Gestion menu : Utilisateur ====================

    while(1) {

        // ==================== Affichager du menu client ====================

        // TEST brute pour choix le mode du menu connecté ou non connecté
        if (1) uxModeDisplay = isUserConnect;

        system("clear");

        menu(uxModeDisplay);

        // ==================== Attente du choix du client ====================

        fpbwhite("▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰\n\n");
        fpbyellow("Choissiez une option"); fpbwhite(" OU "); fpbyellow("ecrivez votre message (si connecté) : ");

        // Affichage de la séquence d'échappement pour la mise en surbrillance
        printf("\033[1;37m");

        fgets(choix_str, sizeof(choix_str), stdin); // Lecture de la ligne entière

        // Réinitialisation des paramètres de mise en forme
        printf("\033[0m");

        system("clear");

        // ==================== Actions par rapport au choix du client ====================

        int choix_int = atoi(choix_str); // Convertir la chaîne de caractères en entier

        if (!isUserConnect){

            switch( choix_int ){ // DECONNECTE
                case 1:
                { // CREATION DE COMPTE
                    printf("================= CREATION DE COMPTE =================\n\n");

                    printf("Creez un nom d'utilisateur : ");
                    scanf("%s", userName);

                    printf("Creez un mot de passe : ");
                    scanf("%s", mdp);


                    printf("\n\n>>>> Compte crée avec succès avec user[ %s ] et mdp[ %s ]\n\n", userName, mdp);
                    printf("======================================================\n\n");

                    getchar();

                    printf("Pour continuer, appuyez sur ENTRER...");
                    getchar();

                    system("clear");

                    break;
                }
                case 2:
                { // CONNEXION AU COMPTE
                    printf("================= CONNEXION AU COMPTE =================\n\n");

                    printf("Entrez votre nom d'utilisateur : ");
                    scanf("%s", userName);

                    printf("Entrez votre mot de passe : ");
                    scanf("%s", mdp);

                    isUserConnect = 1;

                    printf("\n\n>>>> Connexion au serveur fait avec succès");
                    printf("\n\n>>>> Bienvenue, %s", userName);

                    printf("\n\n======================================================\n\n");

                    getchar();

                    printf("Pour continuer, appuyez sur ENTRER...");
                    getchar();

                    system("clear");

                    configTerminal("TERMINAL - Client chat", 100, 35); // Configuration console

                    system("clear");

                    creerPipe();

                    break;
                }
                case 3:
                { // QUITTER

                    system("pkill -f afficheur_message"); // Fermer le processus fils
                    exit(EXIT_SUCCESS);
                    break;
                    
                    system("clear");
                    exit( EXIT_SUCCESS );  
                    system("clear"); 
                }
                default:
                { // ERROR

                    fpbred("\nATTENTION : pas de message vous êtes pas connectés !\n\n");
                    
                    printf("Pour continuer, appuyez sur ENTRER...");
                    getchar();

                    break;
                }
            }
        } else {
            switch( choix_int ){ // CONNECTE
                case 1:
                { // CREATION DE COMPTE
                    printf("================= CREATION DE COMPTE =================\n\n");

                    printf("Creez un nom d'utilisateur : ");
                    scanf("%s", userName);

                    printf("Creez un mot de passe : ");
                    scanf("%s", mdp);


                    printf("\n\n>>>> Compte crée avec succès avec user[ %s ] et mdp[ %s ]\n\n", userName, mdp);
                    printf("======================================================\n\n");

                    getchar();

                    printf("Pour continuer, appuyez sur ENTRER...");
                    getchar();

                    system("clear");

                    break;
                }
                case 2:
                { // DECONNEXION DU COMPTE
                    printf("================= DECONNEXION AU COMPTE =================\n\n");

                    printf("Entrez votre nom d'utilisateur : ");
                    scanf("%s", userName);

                    printf("Entrez votre mot de passe : ");
                    scanf("%s", mdp);

                    isUserConnect = 0;

                    printf("\n\n>>>> Deconnexion au serveur fait avec succès");
                    printf("\n\n======================================================\n\n");

                    getchar();

                    printf("Pour continuer, appuyez sur ENTRER...");
                    getchar();

                    system("clear");

                    break;
                }
                case 3: 
                { // AFFICHAGE LISTE
                    printf("================= LISTE DES UTILISATEURS CONNECTES =================\n\n");

                    printf("\n>> Pierre");
                    printf("\n>> Hugo");
                    printf("\n>> Camille");
                    printf("\n>> Jacques");
                    printf("\n>> Michel");
                    printf("\n>> Manon");

                    printf("\n\n======================================================\n\n");

                    getchar();

                    printf("Pour continuer, appuyez sur ENTRER...");
                    getchar();

                    system("clear");

                    break;
                }
                case 4:
                { // SUPPRESSION COMPTE
                    printf("================= SUPPRESSION D'UN COMPTE =================\n\n");

                    printf("Entrez le nom d'utilisateur du compte : ");
                    scanf("%s", userName);

                    printf("Entrez le mot de passe du compte : ");
                    scanf("%s", mdp);

                    printf("\n\n>>>> Suppression du compte fait avec succès");
                    printf("\n\n======================================================\n\n");

                    getchar();

                    printf("Pour continuer, appuyez sur ENTRER...");
                    getchar();

                    system("clear");

                    break;
                }
                case 5:
                { // QUITTER
                    
                    // Ouvrir le fichier contenant le PID
                    FILE *pid_file = fopen("afficheur_message.pid", "r");
                    if (pid_file == NULL) {
                        perror("Impossible d'ouvrir le fichier PID");
                        exit(EXIT_FAILURE);
                    }

                    // Lire le PID depuis le fichier
                    int pid_afficheur_message;
                    fscanf(pid_file, "%d", &pid_afficheur_message);

                    // Fermer le fichier
                    fclose(pid_file);

                    // Envoyer un signal de terminaison au processus afficheur_message
                    if (kill(pid_afficheur_message, SIGTERM) == -1) {
                        perror("Erreur lors de l'envoi du signal");
                        exit(EXIT_FAILURE);
                    } else {
                        printf("Signal de terminaison envoyé au processus afficheur_message.\n");
                    }
                    
                    system("clear");
                    exit( EXIT_SUCCESS );   
                }
            }
        }

        // Créer le thread de réception
        pthread_create(&thread_recv, NULL, recv_thread, NULL);

        while (1) {
            // ==================== Envoi du message par socket TCP à communication ====================
            printf("Ecrivez le message : ");
            fgets(message, sizeof(message), stdin);
            send(sock, message, strlen(message), 0);

            // Autres tâches du thread principal
            // ...
        }

        // Attendre la fin du thread de réception
        pthread_join(thread_recv, NULL);

    }

    return 0;
}
