#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>

#include "../../src/includes/configTerminal.c"

#define PIPE_NAME "pipe_client_display"
#define BUFFER_SIZE 1024

// ======================== GLOBAL =========================

// Variable globale pour indiquer si un signal SIGTERM a été reçu
int sigterm_received = 0;
int isUpdateStatusConnexion = 0;
int isPipeConnect = 0;
int isMessageAffiche = 0;

// ======================== IMPLEMENTATION FONCTIONS =========================

void chat_group_test(){

    sleep(3);

    system("clear");
    printf("\n\t\t\033[1;32m~~~~~~ Vous êtes connecté au groupe tchat ~~~~~~\033[0m\n");
    printf("\n\033[0;33mAfficheur de messages démarré. Attente de messages...\033[0m\n\n");
    sleep(3);
    printf("Mark: Hello :)\n");
    sleep(2);
    printf("Paul: Ca va ?\n");
    sleep(2);
    printf("Marie: Salut tout le monde !\n");
    sleep(2);
    printf("Paul: Hey, Marie\n");
    sleep(2);

    system("clear");
    printf("\n\t\t\033[1;31m~~~~~~ Vous êtes pas connecté au groupe tchat ~~~~~~\033[0m\n");
    printf("\n\n\033[0;33mAfficheur de messages en attente de connexion...\033[0m\n\n");
    sleep(3);

}

// ======================== MAIN =========================

int main() {

    // ========== Configuration Globale ==========
    configTerminal("TERMINAL - Afficheur Message",100,15); // Configuration console
    system("clear");

    int pipe_fd;
    char buffer[BUFFER_SIZE];

    isUpdateStatusConnexion = 1;

    while (1) {
            
        if(isPipeConnect){

            system("clear");

            printf("\n\t\t\033[1;32m~~~~~~ Vous êtes connecté au groupe tchat ~~~~~~\033[0m\n");

            // Ouvrir le pipe en lecture
            pipe_fd = open(PIPE_NAME, O_RDONLY);
            if (pipe_fd == -1) {
                perror("open");
                exit(EXIT_FAILURE);
            }

            while ((access(PIPE_NAME, F_OK) != -1)){
                // Lire les messages du pipe en boucle
                while (read(pipe_fd, buffer, BUFFER_SIZE) > 0) {
                    printf("Message reçu du client_tchat : %s\n", buffer);
                }
            }
            
            isPipeConnect = 0;

        }else{

            system("clear");

            printf("\n\t\t\033[1;31m~~~~~~ Vous êtes pas connecté au groupe tchat ~~~~~~\033[0m\n");

            while ((access(PIPE_NAME, F_OK) == -1));
            isPipeConnect = 1;

        }

        // Attendre avant de vérifier à nouveau
        sleep(1);
    }

    // Fermer le descripteur de fichier du pipe (ceci ne sera pas atteint)
    close(pipe_fd);

    return 0;
}