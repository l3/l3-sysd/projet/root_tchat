// communication.h
#ifndef COMMUNICATION_H
#define COMMUNICATION_H

// ======================== CONST =========================

#define PORT 4444
#define MAX_MESSAGE_SIZE 256 // Taille maximale d'un message
#define PIPE_NAME "file_message" // Nom du pipe nommé

// ====================== STRUCTURES ======================

// Buffer pour le message
#define MAX_BUFFER_SIZE 1024

// ====================== FONCTIONS ======================

// Fonction qui creer le FD avec une option
int createPipeFD(int option);

// Fonction qui recois depuis le serveur la reponse
void receiveByServer();

// Fonction qui envoie par le pipe les requete du client au Gestion Requete
void sendRequestToGR();

// Fonction qui envoie les messages à tout les utilisateurs
void sendToOnlineUser();

// Fonction qui envoie la réponse à l'utilisateur
void replyToUser();

#endif
