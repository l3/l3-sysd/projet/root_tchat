#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "main.h"

int main() {
    
    // Créer le pipe nommé
    if (mkfifo(PIPE_NAME, 0666) == -1) {
        perror("Erreur lors de la création du pipe nommé");
        exit(EXIT_FAILURE);
    }

    while (1) {

        // Attendre que l'utilisateur appuie sur Entrée
        printf("Cliquez sur Entrée pour fermer le pipe...");
        getchar();

        // Supprimer le pipe nommé
        if (unlink(PIPE_NAME) == -1) {
            perror("Erreur lors de la suppression du pipe nommé");
            exit(EXIT_FAILURE);
        }
        
        printf("Pipe fermé.\n");
        break;
        
    }


    return 0;
}
