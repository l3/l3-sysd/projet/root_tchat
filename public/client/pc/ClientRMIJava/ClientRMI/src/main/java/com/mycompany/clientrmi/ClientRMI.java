package com.mycompany.clientrmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import com.mycompany.gestioncompteimpl.ICompte;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClientRMI {
    private static DatagramSocket _socket;
    private static String _ipUDP;
    private static int _portUDP;
    
    private static void traiterMessageCreateCompte(String message, String ip, int port) throws IOException {
        String[] parts = message.split(" ");
        if (parts.length == 3) {
            String username = parts[1];
            String password = parts[2];

            try {
                Registry registry = LocateRegistry.getRegistry(ip, port);
                ICompte gestionCompte = (ICompte) registry.lookup("GestionCompte");
                boolean compteCree = gestionCompte.creerCompte(username, password);
                if (compteCree) {
                    // Compte créé avec succès
                    String response = "CC '" + username + "' créé avec succès.";
                    // Créer le DatagramPacket avec le message de succès
                    byte[] responseData = response.getBytes();
                    InetAddress serverAddress = InetAddress.getByName(ClientRMI._ipUDP);
                    DatagramPacket packet = new DatagramPacket(responseData, responseData.length, serverAddress, ClientRMI._portUDP);
                    // Envoyer le paquet via le socket UDP
                    ClientRMI._socket.send(packet);
                    System.out.println("Compte '" + username + "' créé avec succès.");
                } else {
                    System.out.println("Échec de la création du compte '" + username + "'.");
                }                                    
            } catch (NotBoundException | RemoteException e) {
                System.err.println("Erreur : " + e);
            }   
        } else {
            System.err.println("Format de message incorrect pour CREER_COMPTE");
        }
    }

 
    private static void traiterMessageConnexion(String message, String ip, int port) throws IOException{
        String[] parts = message.split(" ");
        if (parts.length == 3) {
            String username = parts[1];
            String password = parts[2];
            try {
                Registry registry = LocateRegistry.getRegistry(ip, port);
                ICompte gestionCompte = (ICompte) registry.lookup("GestionCompte");
                boolean isConnected = gestionCompte.connexion(username, password);
                if (isConnected) {
                    String response = "CX réussie pour le compte" + username + "'.";
                    // Créer le DatagramPacket avec le message de succès
                    byte[] responseData = response.getBytes();
                    InetAddress serverAddress = InetAddress.getByName(ClientRMI._ipUDP);
                    DatagramPacket packet = new DatagramPacket(responseData, responseData.length, serverAddress, ClientRMI._portUDP);
                    // Envoyer le paquet via le socket UDP
                    ClientRMI._socket.send(packet);
                    System.out.println("Connexion réussie pour le compte '" + username + "'.");
                } else {
                    System.out.println("Échec de la connexion pour le compte '" + username + "'.");
                }
            } catch (NotBoundException | RemoteException e) {
                System.err.println("Erreur : " + e);
            }
        } else {
            System.err.println("Format de message incorrect pour CONNEXION");
        }
    }
    
   
    private static void traiterMessageSupprimerCompte(String message, String ip, int port) throws IOException{
        String[] parts = message.split(" ");
        if (parts.length == 3) {
            String username = parts[1];
            String password = parts[2]; // Accéder au deuxième élément pour obtenir le mot de passe
            try {
                Registry registry = LocateRegistry.getRegistry(ip, port);
                ICompte gestionCompte = (ICompte) registry.lookup("GestionCompte");
                boolean compteSupprime = gestionCompte.supprimerCompte(username, password);
                if (compteSupprime) {
                    String response = "CS '" + username + "' supprimé avec succès.";
                    // Créer le DatagramPacket avec le message de succès
                    byte[] responseData = response.getBytes();
                    InetAddress serverAddress = InetAddress.getByName(ClientRMI._ipUDP);
                    DatagramPacket packet = new DatagramPacket(responseData, responseData.length, serverAddress, ClientRMI._portUDP);
                    // Envoyer le paquet via le socket UDP
                    ClientRMI._socket.send(packet);
                    System.out.println("Compte '" + username + "' supprimé avec succès.");
                } else {
                    System.out.println("Échec de la suppression du compte '" + username + "'.");
                }
            } catch (NotBoundException | RemoteException e) {
                System.err.println("Erreur : " + e);
            }
        } else {
            System.err.println("Format de message incorrect pour SUPPRIMER_COMPTE");
        }
    }

        
    private static void connectToGestionRequete(String clientIP, int clientPortRMI, String serverIP, int serverPortUDP) {
    Thread udpThread = new Thread(() -> {
        try (DatagramSocket socket = new DatagramSocket()) {
            InetAddress serverAddress = InetAddress.getByName(serverIP);
            ClientRMI._socket = socket;

            byte[] buffer = "Test client RMI".getBytes();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, serverAddress, serverPortUDP);
            socket.send(packet);
            System.out.println("Test Client RMI send");

            while (true) {
                byte[] responseBuffer = new byte[1024];
                DatagramPacket responsePacket = new DatagramPacket(responseBuffer, responseBuffer.length);
                socket.receive(responsePacket);
                String response = new String(responsePacket.getData(), 0, responsePacket.getLength());
                System.out.println("Réponse du serveur : " + response);

                // Découper la requête en parties
                String[] parts = response.split(" ");

                if (parts.length < 1) {
                    System.err.println("Format de message incorrect.");
                    continue; // Passer à la prochaine itération de la boucle
                }

                // Traitement en fonction du type de requête
                switch (parts[0]) {
                    case "CREER_COMPTE":
                        if (parts.length >= 3) {
                            // Si la requête est de type CREER_COMPTE et contient le pseudo et le mot de passe
                            // Traitement du message "CREER_COMPTE"
                            traiterMessageCreateCompte(response, clientIP, clientPortRMI);
                        } else {
                            System.err.println("Format de message incorrect pour CREER_COMPTE");
                        }
                        break;
                    case "CONNEXION_COMPTE":
                        if (parts.length >= 3) {
                            // Si la requête est de type CONNEXION et contient le pseudo et le mot de passe
                            // Traitement du message "CONNEXION"
                            traiterMessageConnexion(response, clientIP, clientPortRMI);
                        } else {
                            System.err.println("Format de message incorrect pour CONNEXION_COMPTE");
                        }
                        break;
                    case "SUPPRIMER_COMPTE":
                        if (parts.length >= 3)  {
                            // Si la requête est de type SUPPRIMER_COMPTE et contient le pseudo
                            // Traitement du message "SUPPRIMER_COMPTE"
                            traiterMessageSupprimerCompte(response, clientIP, clientPortRMI);
                        } else {
                            System.err.println("Format de message incorrect pour SUPPRIMER_COMPTE");
                        }
                        break;
                    default:
                        System.err.println("Requête non reconnue : " + response);
                        break;
                }

            }

        } catch (IOException e) {
            System.err.println("Erreur : " + e);
        }
    });
    udpThread.start();
}


public static void main(String[] args) {
    if (args.length != 4) {
        System.err.println("Usage: java ClientRMI <adr_GestCompImpl> <port_RMI> <adr_Gest_req> <port_UDP>");
        System.exit(1);
    }

    String clientIP = args[0];
    int clientPortRMI = Integer.parseInt(args[1]);
    String serverIP = args[2];
    int serverPortUDP = Integer.parseInt(args[3]);
    ClientRMI._ipUDP = serverIP;
    ClientRMI._portUDP = serverPortUDP;

    // Appel de la méthode pour se connecter au serveur UDP
    connectToGestionRequete(clientIP, clientPortRMI, serverIP, serverPortUDP);

    }
}
