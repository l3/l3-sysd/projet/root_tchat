#ifndef GESTION_REQUETE_H
#define GESTION_REQUETE_H

// ======================== CONST =========================

#define MAX_MESSAGE_SIZE 256 // Taille maximale d'un message
#define MAX_BUFFER_SIZE 1024
#define PIPE_NAME "file_message" // Nom du pipe nommé
#define PORT 8080

// ====================== STRUCTURES ======================

// Structure pour les paramètres du socket UDP
struct UDPSocketParams {
    int sockfd;
    int client_socket; // Descripteur de socket du client UDP
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    socklen_t client_addr_len;
    int client_id; // ID du client
    // Ajoutez d'autres champs pour stocker les informations supplémentaires sur le client si nécessaire
};

// ====================== FONCTIONS ======================

// Fonction d'initialisation du socket UDP
void initUDPSocket(struct UDPSocketParams *udpParams);
// Fonction qui envoi les requêtes dans le socket UDP
void traitementMessagePipe(char *message, int clientSocket, struct sockaddr_in client_addr);
// Fonction qui lis la requête envoyée par communication.c
void* pipe_thread(void* arg);
// Thread qui reçois le message de confirmation du clientrmi
void* client_thread(void* arg);
// Fonction qui envoi le message de confirmation dans le pipe au communication
void EnvoyerMessagePipe(char* action, char* username);
#endif
