#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H

// ======================== CONST =========================

#define MAX_MESSAGE_LENGTH 256
#define MAX_USERS 10

// ====================== STRUCTURES ======================

// Structure pour le message partagé
typedef struct {
    int has_message; // Drapeau pour indiquer s'il y a un message
    char message[MAX_MESSAGE_LENGTH]; // Message à partager
} SharedMessage;

// Structure pour les informations sur les utilisateurs connectés
typedef struct {
    int user_id; // Identifiant de l'utilisateur
    // Ajoutez d'autres informations sur l'utilisateur selon vos besoins
} ConnectedUser;

// ====================== FONCTIONS ======================

// Initialisation de la mémoire partagée
void initializeSharedMemory();

// Nettoyage de la mémoire partagée
void cleanupSharedMemory();

// Fonction pour envoyer un message à tous les utilisateurs connectés
void sendToConnectedUsers();

// Fonction pour recevoir des messages des clients
ssize_t receiveFromClients(char *buffer, size_t buffer_size);

// Fonction pour traiter un message reçu
void processMessage(const char *message);

// Fonction pour recevoir des réponses du serveur
void receiveFromServer();

#endif /* SHARED_MEMORY_H */
