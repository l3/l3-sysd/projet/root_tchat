#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "communication.h"
#include "main.h"

int createServerSocket() {
    int server_fd;
    struct sockaddr_in address;
    int opt = 1;

    // Créer une socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Erreur lors de la création de la socket");
        exit(EXIT_FAILURE);
    }

    // Configuration de la socket pour permettre la réutilisation de l'adresse et du port
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Erreur lors de la configuration de la socket");
        exit(EXIT_FAILURE);
    }

    // Configuration de l'adresse et du port du serveur
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Attacher la socket à l'adresse et au port spécifiés
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Erreur lors de l'attachement de la socket");
        exit(EXIT_FAILURE);
    }

    // Mettre la socket en mode écoute
    if (listen(server_fd, 3) < 0) {
        perror("Erreur lors de la mise en écoute de la socket");
        exit(EXIT_FAILURE);
    }

    return server_fd;
}
#if 0
int createPipeFD(int option){

    // ==================== Configuration du pipe ====================

    int pipe_fd;

    // Ouvrir le pipe nommé avec option (argument)
    pipe_fd = open(PIPE_NAME, option);
    if (pipe_fd == -1) {
        perror("Erreur lors de l'ouverture du pipe nommé");
        exit(EXIT_FAILURE);
    }

    return pipe_fd;

}

void sendRequestToGR(){

    int pipe_fd;
    pipe_fd = createPipeFD(O_WRONLY);

    // Écrire le message dans le pipe nommé
    #if 0
    write(pipe_fd, request, strlen(request) + 1);
    #endif

    //TODO...

    close(pipe_fd);

}

void sendToOnlineUser(){

    int pipe_fd;
    pipe_fd = createPipeFD(O_WRONLY);

    // ===================== Envoie du message aux utilisateurs =====================

    //TODO...

    close(pipe_fd);

}

void replyToUser(){

    int pipe_fd;
    pipe_fd = createPipeFD(O_WRONLY);

    //TODO...

    close(pipe_fd);

}

void receiveByServer(){

    int pipe_fd;
    pipe_fd = createPipeFD(O_RDONLY);

    // Création d'un tampon pour stocker le contenu du tube
	char buffer[MAX_BUFFER_SIZE];
	// Lecture du contenu du tube
	read(pipe_fd, buffer, MAX_BUFFER_SIZE);

    if (1/* answer = 0 */) sendToOnlineUser();
    else replyToUser();

    //TODO...

    close(pipe_fd);

}
#endif

int main() {

    while (1) { // Boucle de receptions et envois

        // =================== Reception des requetes du client ===================
        #if 1

        int server_fd, new_socket;
        struct sockaddr_in address;
        int addrlen = sizeof(address);
        char buffer[1024] = {0};

        // Créer et configurer la socket du serveur
        server_fd = createServerSocket();

        // Attendre une connexion entrante
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            perror("Erreur lors de l'acceptation de la connexion");
            exit(EXIT_FAILURE);
        }

        while(1){

            // Lire les données envoyées par le client
            read(new_socket, buffer, 1024);
            printf("Message du client : %s\n", buffer);

            sleep(2);

            // Envoyer une réponse au client
            const char *hello = "Bonjour du serveur";
            send(new_socket, hello, strlen(hello), 0);
            printf("Message envoyé au client\n");

        }


        #endif
        // =================== Traitement des requetes du client ===================
        #if 0

        if (bytes_received > 0){ // Si la socket est pleine

            // Séparer le message s'il contient plusieurs arguments
            char *token;
            char *delimiter = " ";
            token = strtok(received_message, delimiter);
            int arg_count = 0;
            while (token != NULL) {
                // Traitement de chaque argument du message reçu
                printf("Argument : %s\n", token);
                token = strtok(NULL, delimiter);
                arg_count++;
            }

            // Refusionner le message buffer
            memset(received_message, 0, sizeof(received_message));
            strncpy(received_message, original_received_message, sizeof(received_message) - 1);

            // =================== Envoie des données ===================
            
            if (arg_count == 1) sendRequestToGR(); // Envoie de la requete au gestion_requete
            else sendToOnlineUser();
            
        }

        #endif
        // ==================== Reception des reponses serveur ====================
        #if 0
        receiveByServer();

        // Attente avant la prochaine itération
        sleep(1);

        #endif

    }

    return 0;
}
