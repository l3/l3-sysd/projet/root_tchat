#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <errno.h>
#include <arpa/inet.h>
#include <pthread.h>

#include "gestion_requete.h"


void initUDPSocket(struct UDPSocketParams *udpParams) {
    // Création du socket UDP
    if ((udpParams->sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == 0) {
        perror("Erreur lors de la création du socket");
        exit(EXIT_FAILURE);
    }

    // Configuration de l'adresse du serveur
    memset(&udpParams->server_addr, 0, sizeof(udpParams->server_addr));
    udpParams->server_addr.sin_family = AF_INET;
    udpParams->server_addr.sin_port = htons(PORT);
    udpParams->server_addr.sin_addr.s_addr = INADDR_ANY;

    // Liaison du socket à l'adresse et au port spécifiés
    if (bind(udpParams->sockfd, (struct sockaddr *)&udpParams->server_addr, sizeof(udpParams->server_addr)) == -1) {
        perror("Erreur lors de la liaison du socket");
        exit(EXIT_FAILURE);
    }
    
    printf("Serveur en attente sur le port %d...\n", PORT);
}

void traitementMessagePipe(char *message, int clientSocket, struct sockaddr_in client_addr) {
    // Vérifier si le message correspond à la requête CREER_COMPTE
    if (strncmp(message, "CREER_COMPTE", strlen("CREER_COMPTE")) == 0) {
        if (sendto(clientSocket, message, strlen(message), 0, (struct sockaddr *)&client_addr, sizeof(client_addr)) == -1) {
            perror("Erreur lors de l'envoi de la requête");
            // exit(EXIT_FAILURE);
        }
    } else if (strncmp(message, "CONNEXION_COMPTE", strlen("CONNEXION_COMPTE")) == 0) {
        if (sendto(clientSocket, message, strlen(message), 0, (struct sockaddr *)&client_addr, sizeof(client_addr)) == -1) {
            perror("Erreur lors de l'envoi de la requête");
            // exit(EXIT_FAILURE);
        }
    } else if (strncmp(message, "SUPPRIMER_COMPTE", strlen("SUPPRIMER_COMPTE")) == 0) {
        if (sendto(clientSocket, message, strlen(message), 0, (struct sockaddr *)&client_addr, sizeof(client_addr)) == -1) {
            perror("Erreur lors de l'envoi de la requête");
            // exit(EXIT_FAILURE);
        }
    } else {
        // La requête n'est pas de type CREER_COMPTE
        // Vous pouvez ajouter d'autres conditions pour d'autres types de requêtes si nécessaire
        printf("Requête non valide : %s\n", message);
        // Vous pouvez également envoyer un message d'erreur au client via clientSocket
    }
    
}

void* pipe_thread(void* arg) {
    struct UDPSocketParams *udpParams = (struct UDPSocketParams*)arg;
    int clientSocket = udpParams->client_socket;
    struct sockaddr_in client_addr = udpParams->client_addr;

    int pipe_fd;
    char message[MAX_MESSAGE_SIZE];

    // Ouvrir le pipe nommé en lecture
    pipe_fd = open(PIPE_NAME, O_RDONLY);
    if (pipe_fd == -1) {
        perror("Erreur lors de l'ouverture du pipe nommé");
        exit(EXIT_FAILURE);
    }

    fd_set read_fds;
    struct timeval timeout;
    int retval;

    while (1) {
        FD_ZERO(&read_fds);
        FD_SET(pipe_fd, &read_fds);

        // Configuration du timeout à 1 seconde
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        // Vérifier si des données sont disponibles à lire depuis le pipe nommé
        retval = select(pipe_fd + 1, &read_fds, NULL, NULL, &timeout);
        if (retval == -1) {
            perror("Erreur lors de l'appel à select");
            close(pipe_fd);
            exit(EXIT_FAILURE);
        } else if (retval > 0) {
            // Des données sont disponibles à lire depuis le pipe nommé
            if (FD_ISSET(pipe_fd, &read_fds)) {
                ssize_t bytes_read = read(pipe_fd, message, MAX_MESSAGE_SIZE);
                if (bytes_read == -1) {
                    perror("Erreur lors de la lecture du pipe nommé");
                    close(pipe_fd);
                    exit(EXIT_FAILURE);
                } else if (bytes_read == 0) {
                    // Le client est déconnecté, arrêter la boucle
                    printf("Client déconnecté, arrêt de la boucle de lecture.\n");
                    break;
                } else {
                    // Traitement du message reçu depuis le pipe nommé
                    printf("Message reçu depuis le pipe nommé : %s\n", message);
                    traitementMessagePipe(message, clientSocket, client_addr);
                    // TODO:  une fonction de traitement de message qui envoie au thread de server
                }
            }
        }

        // Vérifier si le pipe nommé a été fermé
        struct stat pipe_stat;
        if (fstat(pipe_fd, &pipe_stat) == -1) {
            perror("Erreur lors de la vérification de l'état du pipe nommé");
            close(pipe_fd);
            exit(EXIT_FAILURE);
        }
        if ((pipe_stat.st_mode & S_IFIFO) == 0) {
            printf("Le pipe nommé a été fermé, arrêt de la boucle de lecture.\n");
            break;
        }
    }

    // Fermeture du pipe nommé
    close(pipe_fd);

    // Terminer le thread
    return NULL;
}

void* client_thread(void* arg){
    struct UDPSocketParams *udpParams = (struct UDPSocketParams*)arg;

    // Structure pour stocker l'adresse du client
    struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(client_addr);

    // Buffer pour stocker les données reçues du client
    char buffer[MAX_BUFFER_SIZE];

    while(1) {
        // Réception de la requête du client
        ssize_t recvlen = recvfrom(udpParams->sockfd, buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr*)&client_addr, &client_len);
        if (recvlen <= 0) {
            perror("Erreur lors de la réception de la requête du client");
            continue; // Continue à écouter les requêtes
        }
        buffer[recvlen] = '\0';

        // Affichage de la requête du client
        printf("Client (%s:%d) : %s\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port), buffer);

        // Envoyer le message reçu dans le pipe nommé
        EnvoyerMessagePipe(buffer, inet_ntoa(client_addr.sin_addr)); // Passer les données réelles du client

        // Réponse au client
        sendto(udpParams->sockfd, "Test ok", strlen("Test ok"), 0, (struct sockaddr*)&client_addr, client_len);
        printf("Réponse envoyée.\n");
    }

    // Terminer le thread
    return NULL;
}



void EnvoyerMessagePipe(char* action, char* username) {
    char message[MAX_BUFFER_SIZE];
    
    // Construire le message à envoyer dans le pipe nommé en utilisant les paramètres action et username
    sprintf(message, action, username);

    // Afficher le message à envoyer dans le pipe
    printf("Message à envoyer dans le pipe : \"%s\".\n", message);
    fflush(stdout); // Forcer le vidage du tampon de sortie

    // Attendre 1 seconde pour permettre à l'affichage de se terminer
    sleep(1);

    // Ouvrir le pipe nommé en écriture
    int pipe_fd = open(PIPE_NAME, O_WRONLY);
    if (pipe_fd == -1) {
        perror("Erreur lors de l'ouverture du pipe nommé");
        return;
    }

    // Écrire le message dans le pipe nommé
    if (write(pipe_fd, message, strlen(message)) == -1) {
        perror("Erreur lors de l'écriture dans le pipe nommé");
    }

    // Fermer le pipe nommé
    close(pipe_fd);
}





int main() {
    pthread_t thread_pipe, thread_client;
    int pipe_fd; // Descripteur de fichier pour le pipe nommé
    struct UDPSocketParams udpParams;

    // ------- Pipe
    // Ouvrir le pipe nommé en lecture (mode non-bloquant)
    pipe_fd = open(PIPE_NAME, O_RDONLY | O_NONBLOCK);
    if (pipe_fd == -1) {
        perror("Erreur lors de l'ouverture du pipe nommé");
        exit(EXIT_FAILURE);
    }

    // ------- Serveur
    // Initialisation du socket UDP
    initUDPSocket( & udpParams);



    // ------- Test reception client
    // Structure pour stocker l'adresse du client
    struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(client_addr);

    // Réception de la requête du client
    char buffer[MAX_BUFFER_SIZE];
    ssize_t recvlen = recvfrom(udpParams.sockfd, buffer, MAX_BUFFER_SIZE, 0, (struct sockaddr * ) & client_addr, & client_len);
    if (recvlen <= 0) {
        perror("Erreur lors de la réception de la requête du client");
    }
    buffer[recvlen] = '\0';
    printf("Client (%s:%d) : %s\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port), buffer);
    
    // Réponse au client
    sendto(udpParams.sockfd, "Test ok", strlen("Test ok"), 0, (struct sockaddr *)&client_addr, client_len);
    printf("Réponse envoyée.\n");

    udpParams.client_socket = udpParams.sockfd;
    udpParams.client_addr = client_addr;
    udpParams.client_addr_len = client_len;

    // ------- Thread
    // Créer le thread pour écouter les requêtes du client UDP
    if (pthread_create(&thread_client, NULL, client_thread, (void*)&udpParams) != 0) {
        perror("Erreur lors de la création du thread pour écouter les requêtes du client UDP");
        exit(EXIT_FAILURE);
    }
    // Créer le thread pour la lecture du pipe nommé
    if (pthread_create( & thread_pipe, NULL, pipe_thread, & udpParams) != 0) {
        perror("Erreur lors de la création du thread pour la lecture du pipe nommé");
        exit(EXIT_FAILURE);
    }


    // Attendre la fin du thread client
    pthread_join(thread_client, NULL);

    // Attendre la fin des threads
    pthread_join(thread_pipe, NULL);

    // Fermer le descripteur de fichier du pipe nommé
    close(pipe_fd);

    // Fermeture du socket UDP à la fin du programme
    close(udpParams.sockfd);

    return 0;
}