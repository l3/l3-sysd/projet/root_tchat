/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.gestioncompteimpl;

/**
 *
 * @author schollet
 */
import java.io.*;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GestionCompteImpl extends UnicastRemoteObject implements ICompte {
    
    private static final String FICHIER_COMPTES = "./src/main/java/com/mycompany/gestioncompteimpl/comptes.txt";

    // Une structure de données pour stocker les comptes des utilisateurs
    private final Map<String, String> comptes;

    public GestionCompteImpl() throws RemoteException {
        super();
        this.comptes = new HashMap<>();
        chargerComptes();
    }
    
    // Méthode pour charger les comptes depuis le fichier
    private void chargerComptes() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(FICHIER_COMPTES));
            String ligne;
            while ((ligne = reader.readLine()) != null) {
                String[] tokens = ligne.split(":");
                if (tokens.length == 2) {
                    comptes.put(tokens[0], tokens[1]);
                } else {
                    System.err.println("Erreur: Format de ligne incorrecte dans le fichier comptes.txt");
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Erreur lors du chargement des comptes : Fichier comptes.txt introuvable.");
        }   catch (IOException ex) {
                Logger.getLogger(GestionCompteImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                System.err.println("Erreur lors de la fermeture du fichier comptes.txt : " + e.getMessage());
            }
        }
    }



    
    // Méthode pour sauvegarder les comptes dans le fichier
    private void sauvegarderComptes() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FICHIER_COMPTES))) {
            for (Map.Entry<String, String> entry : comptes.entrySet()) {
                writer.write(entry.getKey() + ":" + entry.getValue()) ;
                writer.newLine();
            }
        } catch (IOException e) {
            System.err.println("Erreur lors de la sauvegarde des comptes : " + e.getMessage());
        }
    }
    @Override
    public boolean creerCompte(String pseudo, String mdp) throws RemoteException {
        // Vérifier si le pseudo est déjà utilisé
        if (comptes.containsKey(pseudo)) {
            System.err.println("Erreur: Pseudo deja utilisé.");
            return false; // Le pseudo est déjà utilisé
        } else {
            // Ajouter le compte
            comptes.put(pseudo, mdp);
            sauvegarderComptes();
            System.out.println("Compte créé avec succès !");
            return true; // ICompte créé avec succès
        }
    }
    @Override
    public boolean supprimerCompte(String pseudo, String mdp) throws RemoteException {
        // Vérifier si le compte existe
        if (comptes.containsKey(pseudo)) {
            // Vérifier si le mot de passe est correct
            if (comptes.get(pseudo).equals(mdp)) {
                // Supprimer le compte
                comptes.remove(pseudo);
                sauvegarderComptes();
                System.out.println("Compte supprimé avec succès !");
                return true; // ICompte supprimé avec succès
            } else {
                System.err.println("Erreur: Mot de passe incorrect, suppresssion impossible.");
                return false; // Mot de passe invalide
            }
        } else {
            System.err.println("Erreur: Le compte n'existe pas, suppresssion impossible.");
            return false; // Pseudo invalide
        }
    }


    @Override
    public boolean connexion(String pseudo, String mdp) throws RemoteException {
        // Vérifier si le compte existe
        if (comptes.containsKey(pseudo)) {
            // Vérifier si le mot de passe est correct
            if (comptes.get(pseudo).equals(mdp)) {
                System.out.println("Vous êtes connecté, Bienvenue :");
                return true;
            } else {
                System.err.println("Erreur: Mot de passe incorrect.");
                return false;
            }
        } else {
            System.err.println("Erreur: Le compte n'existe pas.");
            return false;
        }
    }

    

   
        
    public static void main(String args[]) {
        if (args.length != 1) {
            System.err.println("Usage: java GestionCompteImpl <port>");
            System.exit(1);
        }

        int port = Integer.parseInt(args[0]);

    try {
            // Installer le gestionnaire de sécurité personnalisé
            System.setSecurityManager(new CustomSecurityManager());

            // Créer un objet GestionCompteImpl
            GestionCompteImpl obj = new GestionCompteImpl();

            // Rendre l'objet accessible via RMI
            Registry registry = LocateRegistry.createRegistry(port);
            registry.rebind("GestionCompte", obj);

            //obj.creerCompte("bapt", "666");
            System.out.println("Serveur prêt sur le port " + port + ".");
            } catch (RemoteException e) {
            System.err.println("Erreur du serveur : " + e.toString());



        }
    }



}



