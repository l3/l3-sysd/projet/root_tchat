package com.mycompany.gestioncompteimpl;

import java.security.Permission;

public class CustomSecurityManager extends SecurityManager {
    // Autoriser lecture
    @Override
    public void checkRead(String file) {
        // Autoriser l'accès en lecture à tous les fichiers
    }
    
    // Autoriser ecriture
    @Override
    public void checkWrite(String file) {
        // Autoriser l'accès en écriture à tous les fichiers

    }
    
    // Autoriser .class à distance 
    @Override
    public void checkPermission(Permission perm) {
    }
}